/*
 * Copyright (c) 2020-2021 Advanced Robotics at the University of Washington <robomstr@uw.edu>
 *
 * This file is part of aruw-turret-mcb.
 *
 * aruw-turret-mcb is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * aruw-turret-mcb is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with aruw-turret-mcb.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef OPEN_HOPPER_COMMAND_HPP_
#define OPEN_HOPPER_COMMAND_HPP_

#include "tap/control/command.hpp"

#include "hopper_subsystem.hpp"

namespace control
{
class OpenHopperCommand;

class OpenHopperCommand : public tap::control::Command
{
public:
    explicit OpenHopperCommand(HopperSubsystem* subsystem);

    void initialize() override;

    void execute() override;

    void end(bool) override;

    bool isFinished() const override;

    const char* getName() const override { return "open hopper"; }

private:
    HopperSubsystem* subsystemHopper;
};

}  // namespace control

#endif  // OPEN_HOPPER_COMMAND_OLD_HPP_
