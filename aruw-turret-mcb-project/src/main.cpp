/*
 * Copyright (c) 2020-2021 Advanced Robotics at the University of Washington <robomstr@uw.edu>
 *
 * This file is part of aruw-turret-mcb.
 *
 * aruw-turret-mcb is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * aruw-turret-mcb is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with aruw-turret-mcb.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "tap/architecture/clock.hpp"
#include "tap/architecture/endianness_wrappers.hpp"
#include "tap/architecture/periodic_timer.hpp"
#include "tap/architecture/profiler.hpp"
#include "tap/board/board.hpp"

#include "control/hopper_subsystem.hpp"
#include "control/open_hopper_command.hpp"
#include "modm/architecture/interface/can_message.hpp"
#include "modm/architecture/interface/delay.hpp"

#include "chassis_mcb_can_comm.hpp"
#include "drivers_singleton.hpp"

// For controlling the hopper cover

#ifdef TARGET_STANDARD_2022
static constexpr float SOLDIER_HOPPER_OPEN_PWM = 0.65f;
static constexpr float SOLDIER_HOPPER_CLOSE_PWM = 0.32f;
static constexpr float SOLDIER_PWM_RAMP_SPEED = 0.0005f;
#else
static constexpr float SOLDIER_HOPPER_OPEN_PWM = 0.38f;
static constexpr float SOLDIER_HOPPER_CLOSE_PWM = 0.7f;
static constexpr float SOLDIER_PWM_RAMP_SPEED = 0.0005f;
#endif

control::HopperSubsystem hopperSubsystem(
    DoNotUse_getDrivers(),
    tap::gpio::Pwm::Pin::C3,
    SOLDIER_HOPPER_OPEN_PWM,
    SOLDIER_HOPPER_CLOSE_PWM,
    SOLDIER_PWM_RAMP_SPEED);

control::OpenHopperCommand openHopper(&hopperSubsystem);

// For communicating with the MCB

ChassisMcbCanComm chassisMcbCanComm(DoNotUse_getDrivers());

// For laser

bool laserOn = false;

// for blinking the LED

int blinkCounter = 0;

// Place any sort of input/output initialization here. For example, place
// serial init stuff here.
static void initializeIo(tap::Drivers *drivers);

// Anything that you would like to be called place here. It will be called
// very frequently. Use PeriodicMilliTimers if you don't want something to be
// called as frequently.
static void updateIo(tap::Drivers *drivers);

static void updateHopperCover(tap::Drivers *drivers);
static void updateLaser(tap::Drivers *drivers);

static constexpr tap::gpio::Digital::InputPin LIMIT_SWITCH_INPUT_PIN =
    tap::gpio::Digital::InputPin::C2;

static constexpr float SAMPLE_FREQUENCY = 500.0f;
static constexpr float MAHONY_KP = 0.1f;

int main()
{
    /*
     * NOTE: We are using DoNotUse_getDrivers here because in the main
     *      robot loop we must access the singleton drivers to update
     *      IO states and run the scheduler.
     */
    ::Drivers *drivers = DoNotUse_getDrivers();

    Board::initialize();
    initializeIo(drivers);

    drivers->pwm.setTimerFrequency(tap::gpio::Pwm::Timer::TIMER1, 333);
    hopperSubsystem.initialize();
    drivers->commandScheduler.registerSubsystem(&hopperSubsystem);

    tap::arch::PeriodicMilliTimer mainLoopTimeout(1000.0f / SAMPLE_FREQUENCY);

    while (1)
    {
        // do this as fast as you can
        PROFILE(drivers->profiler, updateIo, (drivers));

        if (mainLoopTimeout.execute())
        {
            PROFILE(drivers->profiler, drivers->bmi088.periodicIMUUpdate, ());

            if (!drivers->digital.read(tap::gpio::Digital::Button))
            {
                drivers->bmi088.requestRecalibration();
            }

            PROFILE(drivers->profiler, drivers->terminalSerial.update, ());
            PROFILE(drivers->profiler, drivers->commandScheduler.run, ());

            PROFILE(drivers->profiler, chassisMcbCanComm.sendIMUData, ());
#if defined(TARGET_HERO)
            PROFILE(
                drivers->profiler,
                chassisMcbCanComm.sendTurretStatusData,
                (LIMIT_SWITCH_INPUT_PIN));
#endif
            PROFILE(drivers->profiler, updateHopperCover, (drivers));
            PROFILE(drivers->profiler, updateLaser, (drivers));
            PROFILE(drivers->profiler, chassisMcbCanComm.sendSynchronizationRequest, ());
        }

        modm::delay_us(10);
    }
    return 0;
}

static void initializeIo(tap::Drivers *drivers)
{
    drivers->can.initialize();
    drivers->leds.init();
    drivers->digital.init();
    drivers->pwm.init();
    drivers->bmi088.initialize(SAMPLE_FREQUENCY, MAHONY_KP, 0.0f);
#if defined(TARGET_SENTRY)
    drivers->bmi088.setOffsetSamples(4000);
#endif
    drivers->errorController.init();
    drivers->terminalSerial.initialize();
    chassisMcbCanComm.init();
}

static void updateIo(tap::Drivers *drivers) { drivers->canRxHandler.pollCanData(); }

static void updateHopperCover(tap::Drivers *drivers)
{
    if (chassisMcbCanComm.getOpenHopperRequested() &&
        !drivers->commandScheduler.isCommandScheduled(&openHopper))
    {
        drivers->commandScheduler.addCommand(&openHopper);
    }
    else if (
        !chassisMcbCanComm.getOpenHopperRequested() &&
        drivers->commandScheduler.isCommandScheduled(&openHopper))
    {
        drivers->commandScheduler.removeCommand(&openHopper, false);
    }
}

static void updateLaser(tap::Drivers *drivers)
{
    if (!chassisMcbCanComm.getLaserOnRequested() && laserOn)
    {
        drivers->digital.set(tap::gpio::Digital::OutputPin::Laser, false);
        laserOn = false;
    }
    else if (chassisMcbCanComm.getLaserOnRequested() && !laserOn)
    {
        drivers->digital.set(tap::gpio::Digital::OutputPin::Laser, true);
        laserOn = true;
    }
}
