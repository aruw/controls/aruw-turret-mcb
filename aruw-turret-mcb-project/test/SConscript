# Copyright (c) 2020-2021 Advanced Robotics at the University of Washington <robomstr@uw.edu>
#
# This file is part of aruw-turret-mcb.
#
# aruw-turret-mcb is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# aruw-turret-mcb is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with aruw-turret-mcb.  If not, see <https://www.gnu.org/licenses/>.

Import("env")
Import("args")
Import("sources")

if args["TARGET_ENV"] == "tests":
    env_cpy = env.Clone()

    # Append on the global robot target build flag
    env_cpy.AppendUnique(CCFLAGS=["-D " + args["ROBOT_TYPE"]])

    if args["COMPILE_SRC"]:
        rawSrcs = env_cpy.FindSourceFiles(".")
    else:
        rawSrcs = []

    for source in rawSrcs:
        sources.append(env_cpy.Object(source))

Return('sources')
