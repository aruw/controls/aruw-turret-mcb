/*
 * Copyright (c) 2020-2021 Advanced Robotics at the University of Washington <robomstr@uw.edu>
 *
 * This file is part of aruw-turret-mcb.
 *
 * aruw-turret-mcb is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * aruw-turret-mcb is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with aruw-turret-mcb.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef UTIL_MACROS_HPP_
#define UTIL_MACROS_HPP_

/**
 * Define a helper macro that makes it easier to specify at compile time something that should be
 * true for all soldiers.
 */
#if defined(TARGET_SOLDIER_2021) || defined(TARGET_SOLDIER_2022)
#define ALL_SOLDIERS
#endif

#endif  // ARUWSRC_UTIL_MACROS_HPP_
